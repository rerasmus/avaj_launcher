#!/bin/sh
javac avaj/*.java
javac avaj/aircrafts/*.java
javac avaj/weather/*.java

DGRAY='\033[1;30'
LGREEN='\033[1;32m'
LPURP='\033[1;35m'
LCYAN='\033[1;36m'
NC='\033[0m' # No Color

echo "----------------------------------------"
echo "${LPURP}ScenarioIncorrectRuntime.txt${NC}"
cat		ScenarioIncorrectRunTime.txt
java	avaj/Simulator ScenarioIncorrectRunTime.txt
echo "${NC}----------------------------------------"
echo "${LPURP}ScenarioIncorrectAirplane.txt${NC}"
cat		ScenarioIncorrectAirplane.txt
java	avaj/Simulator ScenarioIncorrectAirplane.txt

rm avaj/*.class
rm avaj/aircrafts/*.class
rm avaj/weather/*.class
